#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "coap-simple.h"
#include "NTPClient.h"

const char* ssid     = "anggun";
const char* password = "carisendiriya";
IPAddress dev_ip(103, 175, 217, 78);

void callback_response(CoapPacket &packet, IPAddress ip, int port);

#define NTP_OFFSET   0*60*60    // In seconds
#define NTP_INTERVAL 60 * 1000    // In miliseconds
#define NTP_ADDRESS  "0.id.pool.ntp.org"

WiFiUDP udp;
Coap coap(udp);
NTPClient timeClient(udp, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
int value = 0;
// CoAP client response callback
void callback_response(CoapPacket &packet, IPAddress ip, int port) {
  Serial.println("[Coap Response got]");

  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = NULL;

  Serial.println(p);
}

void setup() {
  Serial.begin(9600);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("Setup Response Callback");
  

  // start coap server/client
  coap.response(callback_response);
  coap.start();
  timeClient.begin();

}

void loop() {

  timeClient.update();
  unsigned long epochTime =  timeClient.getEpochTime();
  ++value;
  Serial.println("Send Request");
  snprintf (msg, MSG_BUFFER_SIZE, "hello world #%ld#%ld", value,epochTime);
  // int msgid = coap.get(IPAddress(103, 175, 217, 78), 5683, "hello");
  int msgid = coap.put(IPAddress(103, 175, 217, 78), 5683, "hello", msg);

 
  coap.loop();
}



